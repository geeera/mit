import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MetamaskRoutingModule } from './metamask-routing.module';
import { MetamaskComponent } from './metamask.component';
import {MatButtonModule} from "@angular/material/button";


@NgModule({
  declarations: [
    MetamaskComponent
  ],
  imports: [
    CommonModule,
    MetamaskRoutingModule,
    MatButtonModule
  ]
})
export class MetamaskModule { }
