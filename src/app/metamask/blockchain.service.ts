import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable} from "rxjs";

import detectEthereumProvider from '@metamask/detect-provider';
import {ethers} from "ethers";

export interface IData {
  id: number;
  address: string;
  balance: string;
}

@Injectable({
  providedIn: 'root'
})
export class BlockchainService {
  private provider: any;
  private signer: any;
  private ethereum: any = window.ethereum;
  private sub$ = new BehaviorSubject<IData | null>(null);

  get observable(): Observable<IData | null> {
    return this.sub$.asObservable();
  }

  get accountsExists(): boolean {
    return this.ethereum._state.accounts.length;
  }

  constructor() {
    detectEthereumProvider().then(() => {
      this.provider = new ethers.providers.Web3Provider(this.ethereum);
      this.signer = this.provider.getSigner();

      if (this.accountsExists) {
        this.updateData();
      }
    }).catch(() => {
      console.info('Please install MetaMask!');
    })

    this.ethereum.on('accountsChanged', () => {
      this.updateData();
    })
  }

  updateData() {
    Promise.all([
      this.getBalance(),
      this.getAddress(),
    ]).then(([balance, address]) => {
      const chainId = this.getChanId();
      const formattedBalance = ethers.utils.formatEther(balance)

      this.sub$.next({
        id: chainId,
        balance: formattedBalance,
        address: address
      });
    })
  }

  async getBalance() {
    return await this.signer.getBalance();
  }

  async getAddress() {
    return await this.signer.getAddress();
  }

  getChanId() {
    return this.provider._network.chainId
  }

  async connectEthereum() {
    return await this.ethereum.request({ method:  'eth_requestAccounts' });
  }
}
