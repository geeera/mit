import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'metamask',
    loadChildren: () => import('./metamask/metamask.module').then(m => m.MetamaskModule)
  },
  {
    path: '**',
    redirectTo: '/metamask'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
